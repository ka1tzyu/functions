const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;

    for (let s of (str1 + str2)) {
        if (!'0123456789'.includes(s)) return false;
    }

    if (str1.length < str2.length) {
        let temp = str1;
        str1 = str2;
        str2 = temp;
    }

    let str1a = str1.split('').reverse();
    let str2a = str2.split('').reverse();
    let output = '';

    let carry = false;

    for (let i = 0; i < str1.length; i++) {
        let result;
        if (str1a[i] && str2a[i]) {
            result = parseInt(str1a[i]) + parseInt(str2a[i]);
        } else if (!str2a[i]) {
            result = parseInt(str1a[i]);
        }

        if (carry) {
            result += 1;
            carry = false;
        }

        if (result >= 10) {
            carry = true;
            output += result.toString()[1];
        } else {
            output += result.toString();
        }
    }

    output = output.split('').reverse().join('');

    if (carry) output = '1' + output;

    return output;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let posts = 0;
    let comments = 0;

    for (let p of listOfPosts) {
        if (p.author === authorName) ++posts;
        if (p.comments !== undefined) {
            for (let c of p.comments) {
                if (c.author === authorName) ++comments;
            }
        }
    }

    return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
    let total25bill = 0;
    let total50bill = 0;
    let total100bill = 0;

    for (const person of people) {
        const pBill = +person;

        if (pBill === 25) {
            ++total25bill;
        } else if (pBill === 50) {
            if (total25bill >= 1) {
                --total25bill;
                ++total50bill;
            } else {
                return 'NO';
            }
        } else if (pBill === 100) {
            if ((total50bill >= 1) && (total25bill >= 1)) {
                --total25bill;
                --total50bill;
                ++total100bill;
            } else if (total25bill >= 3) {
                total25bill -= 3;
                ++total100bill;
            } else {
                return 'NO';
            }
        }
    }
    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
